import React, { useState, useEffect } from 'react'
import { ColContainer } from './ColumnStyles'
import Select from 'react-select'
const options = [
  { value: '1', label: 'isPrime' },
  { value: '2', label: 'isFibonacci' },
]

function Column(props) {
  const [number, setNumber] = useState(1)
  const [optionSelect, setOptionSelect] = useState({ value: '1', label: 'isPrime' })
  const [isPrime, setIsprime] = useState(false)

  useEffect(() => {
    setStatusIsprime(number)
  }, [number])

  useEffect(() => {
    setStatusIsprime(number)
  }, [optionSelect.value])

  const checkNagativeNumber = (number) => {
    const isNagative = Math.sign(number) === -1
    return isNagative
  }

  const onhandleChage = (e) => {
    if (e.target && e.target.value) {
      if (checkNagativeNumber(e.target.value)) {
        setNumber(1)
      } else {
        setNumber(Math.ceil(e.target.value))
      }
    }
  }

  const onSelectOption = (e) => {
    setOptionSelect(e)
  }

  const checkPrimeNumber = (number = 0) => {
    for (let i = 2; i < number; i++) if (number % i === 0) return false
    return number > 1
  }

  const setStatusIsprime = (numberAfterCheckNagative) => {
    const primeNumberStatus = checkPrimeNumber(numberAfterCheckNagative)

    if (optionSelect.value === '1') {
      setIsprime(primeNumberStatus)
    }

    if (optionSelect.value === '2') {
      setIsprime(!primeNumberStatus)
    }
  }
  return (
    <ColContainer style={{ backgroundColor: 'red' }}>
      <div className='column-first' style={{ backgroundColor: 'green' }}>
        <input value={number} onChange={onhandleChage} type='number' name='number' />
      </div>
      <div className='column-mid' style={{ backgroundColor: 'yellow' }}>
        <div className='select-container'>
          <Select
            placeholder='Select'
            onChange={onSelectOption}
            options={options}
            value={optionSelect}
          />
        </div>
      </div>
      <div className='column-last' style={{ backgroundColor: 'blue' }}>
        {isPrime.toString()}
      </div>
    </ColContainer>
  )
}

export default Column
